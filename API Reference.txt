###################
### LIGHT API #####
### (/api/light) ##
###################

/named_list
  Returns a json list of light wrappers with their name, description, and if they support autoscanning

/get_wrapper_config/[light wrapper]
  Returns an HTML form to configure the global settings for a light wrapper

/get_light_config/[light wrapper]
  Returns an HTML form to configure a new light of a specific light wrapper

/configure_light/[light wrapper]
  POST the form data from the get_light_config form to add a new light
  Returns 200 when added

/configure_wrapper/[light wrapper]
  POST the form data from the get_global_config form to set the global settings for a wrapper
  Returns 200 when added

/get_lights
  Returns a json list of lights with their id, name, set delay, and the beat saber lights they are assigned to

/autoscan
  Make a request to here to CLEAR THE EXISTING LIST OF LIGHTS and automatically scan and add new lights
  Returns 200 when done

/remove/[light id]
  Make a request here to remove a light with the specified ID
  Returns 200 when done

/set_delay/[light id]/[delay in seconds]
  Make a request here to set the delay for a light with the specified ID
  Positive delays will make events happen earlier, negative delays will make events happen later
  Returns 200 when done

/assign/[light id]/[beat saber light type]
  Make a request here to assign a Beat Saber light to a light with the specified ID
  The Beat Saber light type should be one of: backLasers, leftLasers, rightLasers, ring, center
  Returns 200 when done

/unassign/[light id]/[beat saber light type]
  Make a request here to remove a Beat Saber light from a light's assignment list
  Returns 200 when done

/test/[light id]
  Make a request here to make a light flash red
  Returns 200 when done

##################
## PLAYER API ####
## (/api/player)##
##################

/get_levels
  Returns a json formatted list of scanned levels

/rescan_levels
  Make a request here to scan for new levels
  Returns 200 when done

/start/[level ID]/[start time]
  Make a request here to start playing a level's lights at a specific time
  Returns a json object with the artist, author, mapper, and name

/stop
  Make a request here to stop the currently playing level
  Returns 200 when done

/[level ID]/album_art.jpg
  Returns the album art of a specific level ID

/[level ID]/audio.ogg
  Returns the audio file for a specific level ID

/change_color/[colorOne or colorTwo]/[color in hex WIHTOUT leading #]
  Make a request here to set the first or second light color
  Returns 200 when done