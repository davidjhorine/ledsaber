# Two Channel GPIO Wrapper: Allows for two digital GPIO pins to control a red and blue light
# If running remotely: PIGPIO_ADDR=<ip> GPIOZERO_PIN_FACTORY=pigpio
import time

META = {
    "SUPPORT_AUTOSCAN" : False,
    "NAME" : "Two Channel GPIO",
    "DESC" : "A red and blue light based off of two digital GPIO pins"
}

GLOBAL_CONFIG_TEMPLATE = [
    {"type": "textOut", "value": "No global configuration for this module"},
]

# Grab the name of the light, the red pin number, and the blue pin number
LIGHT_CONFIG_TEMPLATE = [
    {"type": "textOut", "value":"Name of Light"},
    {"type": "textIn", "value":"lightName"},
    {"type": "textOut", "value":"Red pin"},
    {"type": "textIn", "value":"redPin"},
    {"type": "textOut", "value":"Blue pin"},
    {"type": "textIn", "value":"bluePin"},
]

globalConfig = {}

class Light:
    def __init__(self, config, globalConfig):
        # Importing it when the first light is added avoids import issues later
        import gpiozero
        self.lightName = config['lightName']
        self.delay = 0
        self.assignedTo = []
        self.events = []
        # Define pins based on user input
        self.redPin = gpiozero.LED(int(config['redPin']))
        self.bluePin = gpiozero.LED(int(config['bluePin']))
    def off(self):
        self.redPin.off()
        self.bluePin.off()
    def on(self, color):
        # Turn on the red pin if the color is more red than blue
        if color[0] > color[2]: 
            self.redPin.on()
            self.bluePin.off()
        else: 
            self.bluePin.on()
            self.redPin.off()
    def flash(self, color):
        if color[0] > color[2]:
            # Use a quick flash of the other color to simulate a flash
            self.redPin.on()
            self.bluePin.on()
            time.sleep(.05)
            self.bluePin.off()
        else:
            self.redPin.on()
            self.bluePin.on()
            time.sleep(.05)
            self.redPin.off()
    def fade_black(self, color):
        if color[0] > color[2]:
            self.bluePin.off()
            self.redPin.on()
            time.sleep(.5)
            self.redPin.off()
        else:
            self.bluePin.on()
            self.redPin.off()
            time.sleep(.5)
            self.bluePin.off()