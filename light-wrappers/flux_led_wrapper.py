# Flux LED: Adds support for Flux LED/Magichome devices
from time import sleep
import flux_led

META = {
    "SUPPORT_AUTOSCAN" : True,
    "NAME" : "Flux LED/MagicHome",
    "DESC" : "Control bulbs and LED strips controllable through the Magic Home app"
}

GLOBAL_CONFIG_TEMPLATE = [
    {"type" : "textOut", "value": "There is no global configuration for this module"}
]

LIGHT_CONFIG_TEMPLATE = [
    {"type":"textOut","value":"Light Name"},
    {"type":"textIn","value":"lightName"},
    {"type":"textOut","value":"IP Address"},
    {"type":"textIn","value":"ipAddr"}
]

globalConfig = {}

class Light:
    def __init__(self, config, globalConfig):
        self.lightName = config['lightName']
        self.bulb = flux_led.WifiLedBulb(config['ipAddr'])
        self.delay = 0
        self.assignedTo = []
        self.events = []
    def off(self):
        self.bulb.setRgb(0,0,0, persist=False)
    def on(self, color):
        self.bulb.setRgb(*color, persist=False)
    # Used to add 20 to each RGB value for flashes
    def addFix(self, color):
        newColor = []
        for num in color:
            num += 20
            if num > 255: num = 255
            newColor.append(num)
        return newColor
    def flash(self, color):
        self.bulb.setRgb(*self.addFix(color),persist=False)
        sleep(.1)
        self.bulb.setRgb(*color,persist=False)
    def fade_black(self,color):
        while color[0] + color[1] + color[2] > 0:
            for index in [0,1,2]:
                if color[index] > 0:
                    color[index] -= 20
                    if color[index] < 0:
                        color[index] = 0
            self.bulb.setRgb(*color, persist=False)
        self.bulb.setRgb(0,0,0, persist=False)

def autoscan(globalConfig):
    scanner = flux_led.BulbScanner()
    bulbs = scanner.scan(timeout=5)
    addConfig = []
    for bulb in bulbs:
        addConfig.append({
            "lightName" : bulb['id'],
            "ipAddr" : bulb['ipaddr']
        })
    return addConfig