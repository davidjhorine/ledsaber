# OpenRGB Wrapper: Adds support for openRGB devices
from time import sleep
from openrgb.orgb import OpenRGBClient

from openrgb.utils import RGBColor

META = {
    "SUPPORT_AUTOSCAN" : True,
    "NAME" : "OpenRGB",
    "DESC" : "Controls devices compatible with CalcProgrammer1's OpenRGB"
}

GLOBAL_CONFIG_TEMPLATE = [
    {"type": "textOut", "value" : "After you change this, you should remove all your OpenRGB lights and add them back or rescan."},
    {"type" : "textOut", "value" : "Server IP Address"},
    {"type" : "textIn", "value" : "ipAddr"},
    {"type" : "textOut", "value" : "Port"},
    {"type" : "textIn", "value" : "port"}
]

LIGHT_CONFIG_TEMPLATE = [
    {"type" : "textOut", "value" : "Device ID"},
    {"type" : "textIn", "value" : "deviceId"},
    {"type" : "toggle", "value": "supportsZones"},
    {"type" : "textOut", "value" : "Zone (leave blank if supportsZones is unchecked)"},
    {"type" : "textIn", "value" : "zone"}
]

globalConfig = {
    "ipAddr" : "127.0.0.1",
    "port" : "6742",
}

rgbClient = "unset"
# This function creates a new openRGB client if it does exist, and uses the existing one if it doesn't
def getClient():
    global rgbClient
    global globalConfig
    if rgbClient == "unset":
        from openrgb import OpenRGBClient
        from openrgb.utils import RGBColor
        rgbClient = OpenRGBClient(address=globalConfig['ipAddr'], port=int(globalConfig['port']))
        return rgbClient
    else:
        return rgbClient


class Light:
    def __init__(self, config, globalConfig):
        client = getClient()
        config['deviceId'] = int(config['deviceId'])
        if config['supportsZones']:
            config['zone'] = int(config['zone'])
            self.device = client.devices[config['deviceId']].zones[config['zone']]
        else:
            self.device = client.devices[config['deviceId']]
        self.lightName = self.device.name
        self.delay = 0
        self.assignedTo = []
        self.events = []
    def off(self): self.device.set_color(RGBColor(0,0,0))
    def on(self, color): self.device.set_color(RGBColor(*color))
    # Used to add 20 to each RGB value for flashes
    def addFix(self, color):
        newColor = []
        for num in color:
            num += 20
            if num > 255: num = 255
            newColor.append(num)
        return newColor
    def flash(self,color):
        self.device.set_color(RGBColor(*self.addFix(color)))
        sleep(.1)
        self.device.set_color(RGBColor(*color))
    def fade_black(self,color):
        self.device.set_color(RGBColor(*color))
        while color[0] + color[1] + color[2] > 0:
            for index in [0,1,2]:
                if color[index] > 0:
                    color[index] -= 100
                    if color[index] < 0:
                        color[index] = 0
            self.device.set_color(RGBColor(*color))
        self.device.set_color(RGBColor(0,0,0))

# This function updates the IP Address and port when the global configuration is changed
def globalConfigUpdate():
    global rgbClient
    # If there's no rgbClient registered, there's nothing to update, so just register a new one
    if rgbClient == None: getClient()
    else: rgbClient = OpenRGBClient(address=globalConfig['ipAddr'], port=int(globalConfig['port']))

def autoscan(globalConfig):
    addConfig = []
    client = getClient()
    for device in client.devices:
        if hasattr(device, 'zones'):
            for zone in device.zones:
                addConfig.append({
                    'deviceId' : device.id,
                    'supportsZones' : True,
                    'zone' : zone.id,
                })
        else:
            addConfig.append({
                addConfig.append({
                    'deviceId' : device.id,
                    'supportsZones' : False,
                })
            })
    return addConfig