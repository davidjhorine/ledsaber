# This class provides a simple firewall to allow only certain addresses in
class SecurityCheck:
    def buildIpAddrList(self):
        self.authorizedIpAddrs = []
        with open("Authorized IP Addresses.txt") as authFile:
            for line in authFile:
                # Lines starting with "#" will be treated as comments
                if not line.startswith("#"):
                    line = line.strip('\n') # Remove the newline
                    self.authorizedIpAddrs.append(line)
    def __init__(self):
        self.authorizedIpAddrs = []
        self.buildIpAddrList()
    def check(self, request):
        # This function should return False if the user *is* authorized
        # A true return means "yes, you should abort this request with a 403"
        if request.remote_addr in self.authorizedIpAddrs: return False
        else:
            # If the address isn't in the authorized list, rebuild the list and then check
            # one more time
            self.buildIpAddrList()
            if request.remote_addr in self.authorizedIpAddrs: return False
            else: return True
                

# Create a global SecurityCheck to use
security = SecurityCheck()