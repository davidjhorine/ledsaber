![ledsaber-logo](https://gitlab.com/davidjhorine/ledsaber/-/raw/main/documentation/logo-with-background.png)

# LEDSaber
### Sync your RGB Devices and smart lights to Beat Saber charts
## READ HERE: SAFETY AND DISCLAIMERS
**This program utilizes flashing lights! Those who are affected by flashing lights should not use this program or watch the preview video!**

By using this software, you assume all responsibility for any effects of it, including but not limited to:
* Device damage
* Health issues from flashing lights
* Unauthorized devices changes

Please also know that any files you add to the light-wrappers folder can contain possibly malicious code. Ensure you trust the source of your light-wrappers before using them.

This program is an unofficial program designed to work with custom songs designed for the video game Beat Saber. Neither I nor the files contained within this repository have any official connection to Beat Saber or Beat Games.

## See it in action
Here's a few demo videos showing it in action:
* [Tokyo Machine - Hype (Mapped by Firestrike)](https://youtu.be/eMSypDoIWfA)
* [Mike Morasky/SayMaxWell - Reconstructing Science (Mapped by Qwasyx)](https://youtu.be/PMlbvWvdgu4)
* [C-Show - GIMME DA BLOOD (Mapped by AliceXIV)](https://youtu.be/PaWjQAmP1AI)

## Supported devices
Currently:
* Any device that supports [OpenRGB](https://openrgb.org/)
* Any device controllable through the Magic Home app
* Raspberry Pi GPIO devices using a single pin for red and a single pin for blue

## Installation
* This program requires Python 3 to run. Get it [here](https://www.python.org/downloads/) on Windows or Mac or from your package manager on Linux.

* Download LEDSaber from [here](https://gitlab.com/davidjhorine/ledsaber/-/archive/main/ledsaber-main.zip) and extract the zip file. Alternatively, if you have git installed, you can use `git clone https://gitlab.com/davidjhorine/ledsaber.git`.

* To install the requirements for all included modules, open a terminal and move to the directory where you unzipped the files. Run `python -m pip install -r requirements.txt` (or `python3 -m pip install -r requirements.txt` on Linux). The requirements.txt file is commented with information about which modules are required for which wrappers.

##  Usage
Open a terminal or Powershell window in the folder you unzipped the files. Run `python main.py` (or `python3 main.py` on Linux). A web browser should automatically open. If it doesn't, open your web browser and navigate to http://localhost:52019.

You'll see a UI like this:
![screenshot](https://gitlab.com/davidjhorine/ledsaber/-/raw/main/documentation/Screenshot.png)

On the left side of your screen, you'll see your configuration panel. There, you can change your light colors, configure installed wrappers*, and manage your lights.

* To change a light color, click one of the colored boxes under the colors header.
* To configure a wrapper, click the gear next to its name. 
* To add a light manually, click the + next to the wrapper it uses.
* To add lights automatically, click "Scan for lights"
* To assign a light to a specific Beat Saber light channel, click the button for that light channel under a light's name.
* To change the delay for a light, type a number in seconds in the delay box under a light's name.
* To remove a light, click the garbage can next to the light's name.

On the right side of the screen, you'll see your library. To add new songs, drop them in the "levels" folder and click "Scan songs". To play a song, click the "play song" button under a song in your library.

\* Wrappers are plugins that add a new type of light for LEDSaber to communicate with. You can add new wrappers by placing them in the "light-wrappers" folder. If you know python, you can view instructions for developing your own wrapper [here](https://gitlab.com/davidjhorine/ledsaber/-/blob/main/documentation/wrapper-development.md).

## Advanced usage
* To start the program without launching a web browser, add "headless" as an argument. 

* To allow access to the UI from other devices, add the device's IP address to the "Authorized IP Addresses.txt" file.
