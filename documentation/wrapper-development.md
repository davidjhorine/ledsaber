# Wrapper Development
An example light wrapper that prints to the console is provided [here](https://gitlab.com/davidjhorine/ledsaber/-/blob/main/light-wrappers/print_wrapper.py).

There are three main components to a light wrapper:
* The metadata variables
* The light class
* The optional functions

## The metadata variables
The metadata variables provides LEDSaber with information about your wrapper and how it's configured.

### META
META is a dictionary containing basic information about your wrapper.
```
META = {
    "SUPPORT_AUTOSCAN" : <True or False of if your wrapper has an autoscan function>,
    "NAME" : "<A string of the name your light wrapper should have in the UI>",
    "DESC" : "<A description of what your light wrapper does>"
}
```

### Configuration templates
Configuration templates give information about how your light is configured. GLOBAL_CONFIG_TEMPLATE is shown when configuring a wrapper and contains settings that apply to all lights. LIGHT_CONFIG_TEMPLATE is shown when manually adding a light. Both configuration templates are stored as lists of dictionaries.

Each dictionary in a configuration template has two keys: type and value.

Here's what the different types do:
* `textOut`: displays `value` to the user directly
* `textIn`: provides a textbox and stores the input in the key `value`
* `toggle`: provides a checkbox with the label `value` that stores a boolean in the key `value`

The inputs are provided back in a dictionary of all textIn and toggle values.

For example, this light configuration template:
```
LIGHT_CONFIG_TEMPLATE = [
    {"type": "textOut", "value": "Set the name of this light"},
    {"type": "textIn", "value": "lightName"},
    {"type": "textOut", "value": "This toggle does nothing and is only here for testing"},
    {"type": "toggle", "value": "exampleToggle"},
]
```

would render like this:
![config-template-screenshot](https://gitlab.com/davidjhorine/ledsaber/-/raw/main/documentation/Config-Template-Screenshot.png)

and would return this back to the wrapper:
```
{
    "lightName" : "<user's text>",
    "exampleToggle" : <A true/false value>,
}
```

The global configuration will be stored in a global variable called globalConfig in your wrapper. If you have default values for your global configuration, set them in the globalConfig variable. Otherwise, just make sure globalConfig exists and is set to an empty dictionary.

## The light class
The Light class defines the behavior of your light. A new Light will be created for each light added to the program.

### def__init__(self,config,globalConfig):
Your __init_\_ function will run whenever a new light from your wrapper is created. It will recieve the configuration from the LIGHT_CONFIG_TEMPLATE, the configuration from the GLOBAL_CONFIG_TEMPLATE, and a reference to itself.

Your __init\__ function **must** have:
* An assignment of `self.lightName`, which will be displayed as the light's name in the UI
* An assignment of `self.delay` to the default delay for your light
* An assignment of `self.assignedTo` to an empty list (ie: `self.assignedTo = []`)
* An assignment of `self.events` to an empty list (ie: `self.events = []`)
* Some way of storing the real light te added light is assigned to, such as a reference to an object for the real light (for example: `self.device = lightModule.NewLightDevice(config['deviceAddress'])`) or a unique identifier that can be used to reference the same light.

### def off(self):
Your off function should turn the light off immedietly. On some lights, setting the light to black is faster than turning it off, which is an acceptable way of turning the light off.

### def on(self, color):
Your on function should turn on the light immedietly to the given color. Color is provided as a list of three RGB values 0-255 (for example: `[255,0,0]` for red).

### def flash(self, color):
Your flash function should briefly set the light to a color slightly brighter than the provided color, then set it back to the provided color. I do this in my wrappers by adding 20 to the RGB values, then correcting any number over 255.
self.device.set_color(RGBColor(*color))
### def fade_black(self,color):
Your fade_black function should fade your light from the provided color to black in about half a second. 
Here's how I do it in my wrappers:
```
while color[0] + color[1] + color[2] > 0:
    for index in [0,1,2]:
        if color[index] > 0:
            color[index] -= 100 # Increase or decrease this value to change how fast it fades
            if color[index] < 0:
                color[index] = 0
    # Set device to color here
# Set device to black/turn off device here
```

## Optional functions
These functions aren't required for the wrapper to work, but can be used if needed.

### def globalConfigUpdate():
If this function is defined, it will be called every time your wrapper's global configuration is updated.

### def autoscan(globalConfig):
If SUPPORT_AUTOSCAN is set to True in your META variable, this function will be called when the user clicks "scan lights" in the UI. It should return a list of dictionaries, each dictionary containing the configuration data for a light. For example, with the LIGHT_CONFIG_TEMPLATE defined above, this list:
```
[
    {
        "lightName" : "Autoscanned light #",
        "exampleToggle" : False,
    },
    {
        "lightName" : "Autoscanned light #2",
        "exampleToggle" : False,
    }
]
```
would create two new lights from your Light class and pass each dictionary as the config argument.