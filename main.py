#!/usr/bin/python3
# Purpose: this file handles the webserver and the interactions between the webserver and other services
from flask import Flask, request, abort # The web framework that powers this program
from flask.helpers import send_file, send_from_directory
import threading # Allows for the light tasks and timer to exist without blocking eachother or the main web application
import json, time, os, importlib, sys
# These are the other python files used in the program.
import level_loader
from light_api import light_api, lightWrapperManager
from player_api import player_api, stopSong
from security import security


app = Flask(__name__)

# Register the blueprints from other files
app.register_blueprint(light_api, url_prefix='/api/light')
app.register_blueprint(player_api, url_prefix='/api/player')

# Return the main app page when loaded
@app.route('/')
def returnMainPage():
    if security.check(request): abort(403)
    stopSong()
    return send_file('web/player.html')

# Return any file requested from the assets directory
# This directory will be used to host css, images, javascript, and other things that can
# be safely exposed to anyone.
@app.route('/assets/<string:fileRequested>')
def return_asset(fileRequested):
    # This app should only be usable from the machine running it for now.
    # TODO: Implement a "security code" to allow it to be used from other devices.
    if security.check(request): abort(403)
    try:
        return(send_from_directory('web/assets', fileRequested))
    except FileNotFoundError:
        abort(404)

# Run the app
if __name__ == "__main__":
    # If the "headless" argument isn't passed, open the web page automatically
    if "headless" not in sys.argv:
        import webbrowser
        webbrowser.open('http://localhost:52019', new=0)
    app.run(
        host = '0.0.0.0', # Allows flask to be accessed from other devices
        port = 52019,
        debug = False
    )
    
    