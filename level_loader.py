# Purpose: this file will load the level into a python object that's easier to use in code
import json, os, uuid

# Try to load a scanned_levels.json to avoid rescanning songs every time
if os.path.exists('scanned_levels.json'):
    with open('scanned_levels.json') as scannedLevelsFile:
        levelsList = json.load(scannedLevelsFile)
else:
    levelsList = {}

# Beat saber level object
class BeatSaberLevel:
    def __init__(self, infoFileName, folder):
        with open(folder + '/' + infoFileName) as infoFile:
            info = json.load(infoFile)
        # Grab some basic information about the loaded level
        self.levelName = info['_songName']
        self.artistName = info['_songAuthorName']
        self.levelAuthor = info['_levelAuthorName']
        self.albumArt = info['_coverImageFilename']
        self.bpm = info['_beatsPerMinute']
        self.bps = self.bpm/60
        self.level = info['_difficultyBeatmapSets'][0]['_difficultyBeatmaps'][0]['_beatmapFilename']
        # Convert level format to seconds and events
        self.events = []
        with open(folder + '/' + self.level) as levelFile:
            level = json.load(levelFile)
        EVENT_CHART = {
            '0' : 'backLasers',
            '1' : 'ring',
            '2' : 'leftLasers',
            '3' : 'rightLasers',
            '4' : 'center',
        }
        for event in level['_events']:
            if str(event['_type']) in EVENT_CHART:
                self.events.append({
                    'second': event['_time'] / self.bps,
                    'event' : EVENT_CHART[str(event['_type'])],
                    'value' : event['_value'],
                })

# Scan the levels folder for songs
def scanForLevels(folder):
    folderList = os.listdir(folder)
    levels = {}
    # The info.dat file can have some slight variation in name
    possibleInfoNames = ['info.dat','Info.dat','INFO.dat','INFO.DAT']
    infoFileName = ''
    # This for loop determines which variation of info.dat the name is
    for songFolder in folderList:
        for name in possibleInfoNames:
            if os.path.isfile(folder + '/' + songFolder + '/' + name):
                infoFileName = name
        # If there was no infoFileName chosen, the folder will be skipped and not treated as a level
        if infoFileName != '':
            with open(folder + '/' + songFolder + '/' + infoFileName) as infoFile:
                info = json.load(infoFile)
            # Each level gets a unique ID
            levels[str(uuid.uuid4())] = {
                'folder' : folder + '/' + songFolder,
                'infoFileName' : infoFileName,
                'levelName' : info['_songName'],
                'artistName' : info['_songAuthorName'],
                'levelAuthor' : info['_levelAuthorName'],
                'albumArt' : info['_coverImageFilename'],
                'audio' : info['_songFilename'],
            }
    if os.path.isfile('scanned_levels.json'): os.remove('scanned_levels.json')
    with open('scanned_levels.json','x') as scannedLevelsFile:
        json.dump(levels, scannedLevelsFile, indent=4)
    return(levels)