// This function renders the list of wrappers present on the server
function getWrappers() {
    // Load the data from the server
    fetch('./api/light/named_list')
        .then(function (response) {
            return response.json();
        })
        .then((data) => {
            let wrapperConfig = document.getElementsByClassName('wrapperConfig')[0];
            // Wrapperconfig needs to be empty
            wrapperConfig.innerHTML = "";
            let wrapperTemplate = document.getElementById('wrapperTemplate');
            data.forEach(wrapperObject => {
                // Clone the new wrapper object
                let newWrapper = wrapperTemplate.content.cloneNode(true);
                // Attach the wrapper to the wrappers container
                wrapperConfig.appendChild(newWrapper);
                // Get the appended wrapper
                let appWrapper = wrapperConfig.lastElementChild;
                // Set the name of the wrapper
                appWrapper.getElementsByTagName('p')[0].innerHTML = wrapperObject['name'];
                // Set the ID into a data attribute
                appWrapper.dataset.wrapperId = wrapperObject['wrapperName'];
            });
        })
        .catch(error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This function renders all the lights currently added to the server
function getLights() {
    // Load the data from the server
    fetch('./api/light/get_lights')
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            let lightConfig = document.getElementsByClassName('lightConfig')[0];
            let lightTemplate = document.getElementById('lightTemplate');
            // Empty the lights list
            lightConfig.innerHTML = "";
            data.forEach(lightObject => {
                // Clone and add a new light from the template
                let newLight = lightTemplate.content.cloneNode(true);
                lightConfig.appendChild(newLight);
                // Get the newly added light
                let appLight = lightConfig.lastElementChild;
                // Configure the newly added light's name and ID
                appLight.getElementsByTagName('h3')[0].innerHTML = lightObject['lightName'];
                appLight.dataset.lightId = lightObject['id'];
                // Set ID and default value for light delay
                appLight.querySelector('#lightDelay').value = lightObject['delay'];
                appLight.querySelector('#lightDelay').id = 'lightDelay.' + lightObject['id'];
                appLight.querySelector('#lightDelayLabel').htmlFor = 'lightDelay.' + lightObject['id'];
                appLight.querySelector('#lightDelayLabel').id = 'lightDelayLabel.' + lightObject['id'];
                // Set the ID and default values for the assignment checkboxes
                let checkboxes = ['backLasersCheckbox','leftLasersCheckbox','rightLasersCheckbox','ringCheckbox','centerCheckbox'];
                checkboxes.forEach(checkboxId => {
                    let checkbox = appLight.querySelector('#' + checkboxId);
                    // The lightname is the checkbox ID without the word checkbox
                    let lightname = checkboxId.replace('Checkbox','');
                    // Make the box checked if the light is assigned to that Beat Saber light
                    if (lightObject['assignedTo'].includes(lightname)) {
                        checkbox.checked = true;
                    }
                    // Replace the ID's with unique per-object IDs
                    checkbox.id = checkboxId + "." + lightObject['id'];
                    let label = appLight.querySelector('#' + checkboxId + 'Label');
                    label.htmlFor = checkbox.id;
                    label.id = checkboxId + "Label." + lightObject['id'];
                })
            })
        })
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This function tells the server to scan for lights and then renders the scanned lights
function autoscanLights() {
    let doAutoscan = confirm("Scanning will erase all currently added lights. Are you sure?");
    if (doAutoscan) {
        let loadingScreen = document.getElementById('loadingBox');
        loadingScreen.style.display = 'block';
        fetch('./api/light/autoscan')
          .then (respose =>{
            getLights();
            loadingScreen.style.display = 'none';
          })
          .catch (error => {
              alert("Network error: " + error + ". Try reloading the page or restarting the application.");
          })
    }
}

// This function updates which Beat Saber lights a real light is assigned to
function assignmentUpdate(lightId, bsLight) {
    let checkbox = document.getElementById(bsLight + "Checkbox." + lightId);
    let method = "";
    if (checkbox.checked) {
        method = "assign";
    } else {
        method = "unassign";
    }
    fetch('./api/light/' + method + "/" + lightId + "/" + bsLight)
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This function updates the delay of a light
function delayUpdate(lightId) {
    let lightDelayInput = document.getElementById('lightDelay.' + lightId);
    let delayValue = Number(lightDelayInput.value);
    fetch('./api/light/set_delay/' + lightId + '/' + delayValue)
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This function removes a light when the delete button is clicked
function removeLight(lightId) {
    let doRemove = confirm("Do you really want to remove this light?");
    if (doRemove) {
        fetch('./api/light/remove/' + lightId)
            .catch (error => {
                alert("Network error: " + error + ". Try reloading the page or restarting the application.");
            })
        getLights();
    }
}

// This function tests a light by making it flash red
function testLight(lightId) {
    fetch('./api/light/test/' + lightId)
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This function updates the color used by the lights
function updateColor(whichColor) {
    window.colorRatelimitTimer;
    // Timer doesn't need an initial value, but it needs to exist to prevent clearTimeout from throwing an error
    clearTimeout(window.colorRatelimitTimer);
    // This timeout ensures the color only updates 250ms after the last color change
    window.colorRatelimitTimer = setTimeout(() => {
        color = document.getElementById(whichColor).value;
        color = color.replace('#',''); // Remove the # symbol
        fetch('./api/player/change_color/' + whichColor + '/' + color)
            .catch (error => {
                alert("Network error: " + error + ". Try reloading the page or restarting the application.");
            })
    }, 250)
}

// These functions handle configuration forms
function displayForm(wrapperId, type) {
    let formConfig = document.getElementsByClassName('formConfig')[0];
    let formPanel = document.getElementsByClassName('formPanel')[0];
    let form = formPanel.getElementsByTagName('form')[0];
    fetch('./api/light/get_' + type + '_config/' + wrapperId)
        .then((response) => {
            return response.text();
        })
        .then((data) => {
            form.innerHTML = data;
            form.dataset.type = type;
            form.dataset.id = wrapperId;
            formConfig.style.display = 'block';
        })
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

function closeForm(){
    let formConfig = document.getElementsByClassName('formConfig')[0];
    let form = formConfig.getElementsByTagName('form')[0];
    form.dataset.type = form.dataset.id = "";
    formConfig.style.display = "none";
}

function submitForm(){
    let formConfig = document.getElementsByClassName('formConfig')[0];
    let form = formConfig.getElementsByTagName('form')[0];
    let id = form.dataset.id;
    let type = form.dataset.type;
    let formData = new FormData(form);
    let loadingScreen = document.getElementById('loadingBox');
    loadingScreen.style.display = 'block';
    closeForm()
    fetch('./api/light/configure_' + type + '/' + id, {
        method: 'post',
        body: formData,
    })
        .then(() => {
            getLights();
            loadingScreen.style.display = 'none';
        })
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This function gets the list of songs that have been scanned
function getSongs(){
    let songContainer = document.getElementsByClassName('songs')[0];
    songContainer.innerHTML = "";
    fetch('./api/player/get_levels')
        .then((response) => {
            return response.json();
        })
        .then(data => {
            Object.keys(data).forEach((songId) => {
                // Clone and get a new song node and store the song ID in it
                let songTemplate = document.getElementById('songTemplate').content.cloneNode(true);
                songContainer.appendChild(songTemplate);
                let appSong = songContainer.lastElementChild;
                console.log(appSong)
                appSong.dataset.songId = songId;
                // Set the song title and author and give both unique ID's
                appSong.querySelector('#songTitle').innerHTML = data[songId]['levelName'];
                appSong.querySelector('#songTitle').id = "songTitle." + songId;
                appSong.querySelector('#artistName').innerHTML = data[songId]['artistName'] + " [" + data[songId]['levelAuthor'] + "]";
                appSong.querySelector('#artistName').id = "artistName." + songId;
                // Set the album art
                let albumArtImg = appSong.querySelector('.songHeader img')
                let newAlbumArtImg = new Image();
                newAlbumArtImg.onload = () => {
                    // Replace the blank album art with the real album art when loaded
                    albumArtImg.src = newAlbumArtImg.src;
                }
                newAlbumArtImg.src = "./api/player/" + songId + "/album_art.jpg"
            })
        })
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This function tells the server to scan for new songs and displays the loading screen
function scanSongs(){
    doSongScan = confirm("This might take a bit depending on how many songs you have. Continue?");
    if (doSongScan) {
        let loadingScreen = document.getElementById('loadingBox');
        loadingScreen.style.display = 'block';
        fetch('./api/player/rescan_levels')
            .then(() => {
                getSongs();
                loadingScreen.style.display = 'none';
            })
            .catch (error => {
                alert("Network error: " + error + ". Try reloading the page or restarting the application.");
            })
    }
}

// This function starts song playback and starts the lightshow on the server
function startSong(songId){
    // Get the needed elements and stop any currently playing audio
    let nowPlaying = document.getElementsByClassName('nowPlaying')[0];
    let albumArt = nowPlaying.querySelector('.npHeader img');
    let audioPlayer = document.getElementById('mainAudioPlayer');
    let loadingScreen = document.getElementById('loadingBox');
    audioPlayer.pause();
    audioPlayer.currentTime = 0;
    loadingScreen.style.display = 'block';
    // Set up the new audio player with the audio file
    audioPlayer.src = './api/player/' + songId + '/audio.ogg';
    console.log(audioPlayer.src);
    audioPlayer.addEventListener('canplaythrough', function sendStartEvent() {
        // Trying to have this as an anonymous function removes my ability to remove the event listener later
        audioPlayer.removeEventListener('canplaythrough', sendStartEvent);
        console.log('loaded audio!')
        // Sync 1 second in the future as the starttime for the audio
        startTime = (Date.now()/1000) + 2;
        setTimeout(() => {
            audioPlayer.play();
        }, 2000)
        // Send the starttime to the server and stop the loading screen
        fetch('./api/player/start/' + songId + '/' + startTime)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                document.getElementById('npSongTitle').innerHTML = data['name'];
                document.getElementById('npArtistName').innerHTML = data['artist'] + " [" + data['mapper'] + "]";
                nowPlaying.style.display = 'block';
                loadingScreen.style.display = 'none';
            })
            .catch (error => {
                alert("Network error: " + error + ". Try reloading the page or restarting the application.");
            })
    })
    audioPlayer.addEventListener('ended', stopSong)
    // Start loading the new album art image
    let newAlbumArtImg = new Image();
    newAlbumArtImg.onload = () => {
        // Replace the blank album art with the real album art when loaded
        albumArt.src = newAlbumArtImg.src;
    }
    newAlbumArtImg.src = "./api/player/" + songId + "/album_art.jpg";
    // Start loading the song
    audioPlayer.load();
}

// This function closes the now playing, stops the audio player, and tells the server to stop
function stopSong(){
    let nowPlaying = document.getElementsByClassName('nowPlaying')[0];
    let audioPlayer = document.getElementById('mainAudioPlayer');
    audioPlayer.removeEventListener('ended', stopSong)
    audioPlayer.pause();
    audioPlayer.currentTime = 0;
    nowPlaying.style.display = 'none';
    fetch('./api/player/stop')
        .catch (error => {
            alert("Network error: " + error + ". Try reloading the page or restarting the application.");
        })
}

// This loads everything when the website successfully loads
window.onload = () => {
    getWrappers();
    getLights();
    getSongs();
}