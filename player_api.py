# Purpose: this file handles syncing the web audio to the lighting clock
# and firing lighting events based on the clock, offsets, and light assignments
import threading, os, sys, time
from flask import Blueprint, abort, Flask, request
from flask.helpers import send_file
from light_api import lightWrapperManager
from level_loader import levelsList, BeatSaberLevel, scanForLevels
from flask.json import jsonify
from security import security

# TODO: Better error handling for bad URL's, implement security on new endpoints, add color support, update events list on changes

# Register the player api as a blueprint
player_api = Blueprint('player_api', __name__)

# Build the events list in a different way
def buildEventList(level):
    lightCorrectedEventsList = []
    for event in level.events:
        for light in lightWrapperManager.lights:
            if event['event'] in lightWrapperManager.lights[light].assignedTo:
                lightCorrectedEventsList.append({
                    'id' : light,
                    'second' : event['second'] - lightWrapperManager.lights[light].delay,
                    'value' : event['value']
                })
    return sorted(lightCorrectedEventsList, key=lambda k: k['second'])

# Return a list of all the levels
@player_api.route('/get_levels')
def getLevels():
    if security.check(request): abort(403)
    return(jsonify(
        levelsList
    ))

# Scan the levels folder for new levels
@player_api.route('/rescan_levels')
def scanLevels():
    if security.check(request): abort(403)
    global levelsList
    levelsList = scanForLevels('levels')
    return('200')

# The RunningClock class keeps track of the current clock, translates values into actions,
# and sends these actions to the appropriate lights
class RunningClock:
    def __init__(self):
        self.playing = False
        self.eventsList = []
        self.color1 = [0,0,255]
        self.color2 = [255,0,0]
    def runValue(self, value, id):
        light = lightWrapperManager.lights[id]
        # The valueMap translates the numbered value to a callable function
        valueMap = {
            1: light.on,
            2: light.flash,
            3: light.fade_black,
        }
        if value == 0:
            light.off()
        elif value in [1,2,3]:
            # The color list needs to be copied to prevent the light module
            # from being able to change it. Before adding the copy(), the fade_black
            # function in openrgb_wrapper and flux_led_wrapper would cause all future
            # instructions to use the color [0,0,0].
            valueMap[value](self.color1.copy())
        elif value in [5,6,7]:
            # Values 5-7 do the same thing as values 1-3, but in the second color
            valueMap[value-4](self.color2.copy())
    def start(self, startTime):
        step = 0
        threads = {}
        while step < len(self.eventsList):
            if not self.playing: return('stopped')
            if (time.time() - startTime) >= self.eventsList[step]['second']:
                # Spawning another thread here ensures that waiting for a light t change doesn't block future events
                try:
                    if not threads[self.eventsList[step]['id']].is_alive():
                        # Checking if the previous thread is still alive prevents multiple 
                        # threads from starting and limits threads to one per light
                        threads[self.eventsList[step]['id']] = threading.Thread(target=self.runValue, args=[self.eventsList[step]['value'], self.eventsList[step]['id']])
                        threads[self.eventsList[step]['id']].start()
                except KeyError:
                    # The first event on each light will cause a KeyError because no threads
                    # have been created yet for that light
                    threads[self.eventsList[step]['id']] = threading.Thread(target=self.runValue, args=[self.eventsList[step]['value'], self.eventsList[step]['id']])
                    threads[self.eventsList[step]['id']].start()

                step += 1
        self.playing = False
        return('stopped')

# This RunningClock is shared across many functions
runningClock = RunningClock()

def turnAllOff():
    for light in lightWrapperManager.lights:
        lightWrapperManager.lights[light].off()

# Start a song
@player_api.route('/start/<string:id>/<string:startTime>')
def startSong(id, startTime):
    if security.check(request): abort(403)
    startTime = float(startTime)
    global levelsList
    # Start a thread to turn off all the lights
    threading.Thread(target=turnAllOff).start()
    # Give the running clock half a second to stop if currently playing
    if runningClock.playing:
        runningClock.playing = False
        time.sleep(.5)
    # Load the level with the specified ID into the event list builder
    levelInfo = levelsList[id]
    bsl = BeatSaberLevel(levelInfo['infoFileName'], levelInfo['folder'])
    runningClock.eventsList = buildEventList(bsl)
    # Set the playing variable to true to allow the runningClock to play
    runningClock.playing = True
    # Start the clock on a new thread
    threading.Thread(target=runningClock.start, args=[startTime]).start()
    return jsonify({
        'name' : bsl.levelName,
        'artist' : bsl.artistName,
        'author' : bsl.levelAuthor,
        'mapper' : bsl.levelAuthor,
    })

# Stop a song
@player_api.route('/stop')
def stopSong():
    if security.check(request): abort(403)
    runningClock.playing = False
    return('200')

# Get a song's album art
@player_api.route('/<string:id>/album_art.jpg')
def getAlbumArt(id):
    if security.check(request): abort(403)
    filepath = levelsList[id]['folder'] + "/" + levelsList[id]['albumArt']
    return(send_file(filepath))

# Get a song's audio file
@player_api.route('/<string:id>/audio.ogg')
def getAudio(id):
    if security.check(request): abort(403)
    filepath = levelsList[id]['folder'] + "/" + levelsList[id]['audio']
    return(send_file(filepath))

# Change a color
@player_api.route('/change_color/<string:whichColor>/<string:color>')
def changeColor(whichColor, color):
    if security.check(request): abort(403)
    # Thanks to John1024 on Stackoverflow for the code to convert a hex color to RGB
    rgbColor = list((int(color[i:i+2], 16) for i in (0, 2, 4)))
    if whichColor == "colorOne": runningClock.color1 = rgbColor.copy()
    elif whichColor == "colorTwo": runningClock.color2 = rgbColor.copy()
    else: abort(404)
    return('200')