# Print Wrapper: Adds virtual lights that print to the console, for debugging purposes
from time import sleep

# This contains metadata about your light module
META = {
    "SUPPORT_AUTOSCAN" : True,
    "NAME" : "Print Wrapper",
    "DESC" : "A virtual light that prints to console",
}

# This determines how many lights should be added through autoscanning for autoscanning debug
AUTOSCAN_COUNT = 0

# The GLOBAL_CONFIG_TEMPLATE will allow the user to set things that apply to the entire wrapper
GLOBAL_CONFIG_TEMPLATE = [
    {"type": "textOut", "value": "This does nothing"},
    {"type": "textIn", "value": "doesNothing"},
]

# The LIGHT_CONFIG_TEMPLATE will alow the user to set things that apply per-light
LIGHT_CONFIG_TEMPLATE = [
    {"type": "textOut", "value": "Set the name of this light"},
    {"type": "textIn", "value": "lightName"},
    {"type": "textOut", "value": "This toggle does nothing and is only here for testing"},
    {"type": "toggle", "value": "exampleToggle"},
]

# Just set globalConfig to an empty dictionary, it will be used to store the global configuration
# Alternatively, you can fill in globalConfig will default values
globalConfig = {}

# Your module should register a class called Light
class Light:
    # Your __init__ function will recieve the config as an argument
    def __init__(self, config, globalConfig):
        # lightName will be shown to the user
        self.lightName = config['lightName']
        self.exampleToggle = config['exampleToggle']
        # Set the "delay" to a default delay for your light module
        self.delay = 0
        # Set assignedTo and events to an empty list
        self.assignedTo = []
        self.events = []
    # Your off function should immediately turn off the light
    def off(self):
        print("Turned off " + self.lightName)
    # Your on function should immediately turn the light on to the specied color, provided as a list containing RGB values 0-255
    def on(self, color):
        print("Turned on " + self.lightName + " to R:" + str(color[0]) + " G: " + str(color[1]) + " B: " + str(color[2]))
    # Your flash function should flash the light slightly brighter to the provided color, the change it to the provided color
    def flash(self, color):
        print("Flashed " + self.lightName + " to R:" + str(color[0] + 20) + " G: " + str(color[1] + 20) + " B: " + str(color[2] + 20))
        sleep(.1)
        print("End flash on " + self.lightName + " to R:" + str(color[0]) + " G: " + str(color[1]) + " B: " + str(color[2]))
    # Your fade_black function should fade the bulb from the provided color to black over the course of about half a second
    def fade_black(self, color):
        print("Begin fade " + self.lightName + " to R:" + str(color[0]) + " G: " + str(color[1]) + " B: " + str(color[2]))
        sleep(.5)
        print("End fade " + self.lightName)

def globalConfigUpdate():
    print("Global config set to " + str(globalConfig))

def autoscan(globalConfig):
    # Autoscan should return a list of configuration dictionaries
    i = 0
    addConfig = []
    while i < AUTOSCAN_COUNT:
        # This sleep simulates the time it'd take for a real light to scan
        # Please don't add sleeps to your real light wrapper's autoscan.
        sleep(.25)
        addConfig.append({
            "lightName" : "Autoscanned light #" + str(i),
            "exampleToggle" : False,
        })
        i += 1
    return addConfig