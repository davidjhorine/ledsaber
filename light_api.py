# Purpose: this file handles tracking the lights and 
# provides an API for interacting with the lights list
import threading, importlib, os, sys, time
from flask import Blueprint, abort, Flask, request
from flask.json import jsonify
# UUID is used to give lights a unique identifier
import uuid
# Get the security check from the main file
from security import security

# Register the light API as a blueprint
light_api = Blueprint('light_api', __name__)

# The LightWrapperManager class will handle importing and managing all the
# available light wrappers.
class LightWrapperManager:
    def __init__(self):
        self.wrappers = {}
        self.lights = {}
        wrapperFiles = os.listdir('light-wrappers')
        # The wrappers can't be imported unless the sys.path includes the light-wrappers directory
        sys.path.append('light-wrappers')
        for wrapperFile in wrapperFiles:
            if wrapperFile.endswith('.py'): # Make sure the wrapper file is actually a python file
                wrapperFile = wrapperFile[:-3] # Trim off the .py extension
                # The 'wrappers' variable should contain a dictionary that looks like
                # {'nameOfWrapperFile',wrapperModuleObject}
                self.wrappers[wrapperFile] = importlib.import_module(wrapperFile)
    # Having a namedList like this will make it easier to display in certain parts of the UI
    def namedList(self):
        namedList = []
        for wrapper in self.wrappers:
            namedList.append({
                'wrapperName' : wrapper,
                'name' : self.wrappers[wrapper].META['NAME'],
                'desc' : self.wrappers[wrapper].META['DESC'],
                'autoscan' : self.wrappers[wrapper].META['SUPPORT_AUTOSCAN']
            })
        return(namedList)
    def namedLightList(self):
        namedLightList = []
        for light in self.lights:
            namedLightList.append({
                'id' : light,
                'lightName' : self.lights[light].lightName,
                'delay' : self.lights[light].delay,
                'assignedTo' : self.lights[light].assignedTo,
            })
        return(namedLightList)

# Register a LightWrapperManager for shared use across files
lightWrapperManager = LightWrapperManager()

# Return the namedList
@light_api.route('/named_list')
def getNamedList():
    if security.check(request): abort(403)
    return(jsonify(
        lightWrapperManager.namedList()
    ))

# Render a config template to HTML
def renderConfigTemplate(config):
    if security.check(request): abort(403)
    renderList = []
    finalRender = ""
    for conf in config:
        # textOut configurations will display text to the user
        if conf['type'] == "textOut":
            renderList.append(
                "<p class=\"configTextOut\">" + conf['value'] + "</p>"
            )
        # textIn configurations will take text from the user
        elif conf['type'] == "textIn":
            renderList.append(
                "<input class=\"configTextIn\" type=\"text\" name=\"" + conf['value'] +"\" id=\"" + conf['value'] +"\"><br>" 
            )
        # toggle configurations will give the user a checkbox
        elif conf['type'] == "toggle":
            renderList.append(
                "<input class=\"configToggle\" type=\"checkbox\" name=\"" + conf['value'] +"\" id=\"" + conf['value'] +"\">" + 
                "\n<label for=\"" + conf['value'] + "\">" + conf['value'] + "</label><br>"
            )
    # Combine a list of HTML lines to render in to one string with newlines
    for line in renderList:
        finalRender += line + "\n"
    # Add a submit button
    # finalRender += "<button onclick=\"submitForm(this.dataset.type, this.dataset.id)\">Finish configuring</button>"
    return finalRender

# Return a config template for a light wrapper
@light_api.route('/get_wrapper_config/<string:requestedWrapper>')
def getGlobalConfig(requestedWrapper):
    if security.check(request): abort(403)
    try:
        globalConfig = lightWrapperManager.wrappers[requestedWrapper].GLOBAL_CONFIG_TEMPLATE
        return(renderConfigTemplate(globalConfig))
    except KeyError: abort(404)
            
# Return a config template for a light
@light_api.route('/get_light_config/<string:requestedWrapper>')
def getLightConfig(requestedWrapper):
    if security.check(request): abort(403)
    try:
        lightConfig = lightWrapperManager.wrappers[requestedWrapper].LIGHT_CONFIG_TEMPLATE
        return(renderConfigTemplate(lightConfig))
    except KeyError: abort(404)
            
# Configure a light based on configuration data from a form
@light_api.route('/configure_light/<string:requestedWrapper>', methods=['POST'])
def configureNewLight(requestedWrapper):
    if security.check(request): abort(403)
    lightConfig = {}
    for conf in lightWrapperManager.wrappers[requestedWrapper].LIGHT_CONFIG_TEMPLATE:
        # Check every value that should have a response based on the configuration template
        if conf['type'] in ['textIn','toggle']:
            lightConfig[conf['value']] = request.form.get(conf['value'], default=None)
    identifier = requestedWrapper + "." + str(uuid.uuid1())
    lightWrapperManager.lights[identifier] = lightWrapperManager.wrappers[requestedWrapper].Light(
        lightConfig,
        lightWrapperManager.wrappers[requestedWrapper].globalConfig,
    )
    return('200')

# Set the global configuration for a wrapper
@light_api.route('/configure_wrapper/<string:requestedWrapper>', methods=['POST'])
def configureWrapper(requestedWrapper):
    if security.check(request): abort(403)
    wrapperConfig = {}
    for conf in lightWrapperManager.wrappers[requestedWrapper].GLOBAL_CONFIG_TEMPLATE:
        # Check every value that should have a response based on the configuration template
        if conf['type'] in ['textIn','toggle']:
            wrapperConfig[conf['value']] = request.form.get(conf['value'], default=None)
    lightWrapperManager.wrappers[requestedWrapper].globalConfig = wrapperConfig
    # This allows wrappers to immedietly execute an action upon the global configuration being updated
    if hasattr(lightWrapperManager.wrappers[requestedWrapper], 'globalConfigUpdate'):
        lightWrapperManager.wrappers[requestedWrapper].globalConfigUpdate()
    return('200')

# Get the list of lights
@light_api.route('/get_lights')
def getLights():
    if security.check(request): abort(403)
    return(jsonify(
        lightWrapperManager.namedLightList()
    ))

# Autoscan for lights
@light_api.route('/autoscan')
def autoscan():
    if security.check(request): abort(403)
    wrappers = lightWrapperManager.wrappers
    newconfigs = []
    # Because the light's identifiers are randomly generated and 
    # many lights wouldn't have consistent identifiers, the best
    # option is to clear the list of lights prior to scanning.
    lightWrapperManager.lights = {}
    for wrapper in wrappers:
        if wrappers[wrapper].META['SUPPORT_AUTOSCAN']:
            newconfigs = wrappers[wrapper].autoscan(lightWrapperManager.wrappers[wrapper].globalConfig)
            if newconfigs != []:
                for lightConfig in newconfigs:
                    identifier = wrapper + "." + str(uuid.uuid4())
                    lightWrapperManager.lights[identifier] = lightWrapperManager.wrappers[wrapper].Light(
                        lightConfig,
                        lightWrapperManager.wrappers[wrapper].globalConfig,
                    )
    return('200')

# Delete a light
@light_api.route('remove/<string:id>')
def removeLight(id):
    if security.check(request): abort(403)
    try:
        lightWrapperManager.lights.pop(id)
        return('200')
    except KeyError:
        abort(404)

# Set the delay for a light
@light_api.route('set_delay/<string:id>/<delay>')
def setLightDelay(id, delay):
    if security.check(request): abort(403)
    try: 
        # Specifying the delay to be a float in the URL structure results in the delay route not accepting integers
        delay = float(delay)
        lightWrapperManager.lights[id].delay = delay
        return('200')
    except KeyError: abort(404)

# Add a light assignment
@light_api.route('/assign/<string:id>/<string:type>')
def assignToLight(id, type):
    if security.check(request): abort(403)
    try:
        if (type in ['backLasers', 'ring', 'leftLasers', 'rightLasers', 'center']) & (type not in lightWrapperManager.lights[id].assignedTo):
            lightWrapperManager.lights[id].assignedTo.append(type)
            return('200')
        else:
            abort(400)
    except KeyError: abort(404)

# Remove a light assignment
@light_api.route('/unassign/<string:id>/<string:type>')
def unassignFromLight(id, type):
    if security.check(request): abort(403)
    try:
        lightWrapperManager.lights[id].assignedTo.remove(type)
        return('200')
    except ValueError: abort(400)
    except KeyError: abort(404)

# Test a light by making it flash
@light_api.route('/test/<string:id>')
def testLight(id):
    if security.check(request): abort(403)
    try:
        lightWrapperManager.lights[id].flash([255,0,0])
        return('200')
    except KeyError: 
        abort(404)